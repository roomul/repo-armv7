#!/bin/sh -e

GUSER="${GUSER:-pi}"
GROUPS="wheel tty tape daemon floppy disk lp dialout audio video utmp adm cdrom optical mail storage scanner network kvm input users"

log(){
	printf " -> $1 \n"
}

main(){
	for group in $GROUPS ; do
		log "Add $GUSER to $group"
		addgroup $GUSER $group
	done
}

main "$@"
